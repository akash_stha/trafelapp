//
//  SlideModel.swift
//  Trafel App
//
//  Created by Newarpunk on 4/3/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import Foundation

struct SlideModel {
    
    let imageName: String
    let title: String
    let description: String
    
    static let collection: [SlideModel] = [
        SlideModel(imageName: "one", title: "Welcome to Trafel", description: "Trafel allows you to trave around the world, make new frineds and create memorable exeperiences."),
        SlideModel(imageName: "two", title: "Connect Socially", description: "Connect across continents to strangers via the app"),
        SlideModel(imageName: "three", title: "Explore the World", description: "Explore around the world and create magic unforgettable moments"),
        SlideModel(imageName: "four", title: "Welcome to Trafel", description: "Trafel allows you to trave around the world, make new frineds and create memorable exeperiences."),
        SlideModel(imageName: "five", title: "Welcome to Trafel", description: "Trafel allows you to trave around the world, make new frineds and create memorable exeperiences."),
        SlideModel(imageName: "six", title: "Welcome to Trafel", description: "Trafel allows you to trave around the world, make new frineds and create memorable exeperiences."),
        SlideModel(imageName: "seven", title: "Welcome to Trafel", description: "Trafel allows you to trave around the world, make new frineds and create memorable exeperiences."),
        SlideModel(imageName: "eight", title: "Welcome to Trafel", description: "Trafel allows you to trave around the world, make new frineds and create memorable exeperiences."),
        SlideModel(imageName: "nine", title: "Welcome to Trafel", description: "Trafel allows you to trave around the world, make new frineds and create memorable exeperiences."),
        SlideModel(imageName: "ten", title: "Welcome to Trafel", description: "Trafel allows you to trave around the world, make new frineds and create memorable exeperiences.")
    ]
    
}
