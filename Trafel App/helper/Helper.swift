//
//  helper.swift
//  Trafel App
//
//  Created by Newarpunk on 4/3/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import Foundation
import UIKit

func delay(durationInSeconds: Double, completion: @escaping () -> Void) {
    DispatchQueue.main.asyncAfter(deadline: .now() + durationInSeconds, execute: completion)
}

//func window(storyboardId: UIViewController) {
//    if let appDelegate = UIApplication.shared.delegate as? AppDelegate,
//        let window = appDelegate.window {
//        window.rootViewController = storyboardId
////        UIView.transition(with: window, duration: 0.30, options: .transitionCrossDissolve, animations: nil, completion: nil)
//    }
//}
