//
//  constants.swift
//  Trafel App
//
//  Created by Newarpunk on 4/3/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import Foundation

struct k {
    
    struct Segue {
        static let showOnboard = "showOnboard"
        static let showLoginSignup = "showLoginSignup" 
    }
    
    struct StoryboardId {
        static let main = "Main"
        static let mainTabBarController = "MainTabBarController"
        static let onboardingViewController = "OnBoardingViewController"
    }
    
}
