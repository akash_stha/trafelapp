//
//  SettingsViewController.swift
//  Trafel App
//
//  Created by Newarpunk on 4/3/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var logout: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    private func setupViews() {
        view.backgroundColor = .blue
        logout.tintColor = .darkGray
    }
    
    @IBAction func logOutButtonTapped(_ sender: UIBarButtonItem) {
        PresenterManager.shared.show(vc: .onBoarding)
    }
}
