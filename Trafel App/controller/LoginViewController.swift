//
//  LoginViewController.swift
//  Trafel App
//
//  Created by Newarpunk on 4/3/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    weak var delegate: OnboardingDelegate?
    private let isSuccesfullLogin = false
    
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var confirmPasswordTextfield: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    
    private enum PageType {
        case login
        case signup
    }
    
    private var errorMessage: String? {
        didSet {
            showErrorMessageIfNeeded(text: errorMessage)
        }
    }
    
    private var currentPageType: PageType = .login {
        didSet {
            // print(currentPageType)
            setupViewsFor(pageType: currentPageType)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        noNavBar()
        setupViewsFor(pageType: .login)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(handleDone))
    }
    
    @objc func handleDone() {
        dismiss(animated: true, completion: nil)
    }
    
    func noNavBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
        
        // For button
        signupButton.layer.cornerRadius = signupButton.frame.height / 2
        loginButton.layer.cornerRadius = loginButton.frame.height / 2
    }

    private func setupViewsFor(pageType: PageType) {
        errorLabel.text = nil
        confirmPasswordTextfield.isHidden = pageType == .login
        signupButton.isHidden = pageType == .login
        forgotPasswordButton.isHidden = pageType == .signup
        loginButton.isHidden = pageType == .signup
    }
    
    @IBAction func segmentedControlChanged(_ sender: UISegmentedControl) {
        currentPageType = sender.selectedSegmentIndex == 0 ? .login : .signup
        // to undetstand the line no. 57
//        print(sender.selectedSegmentIndex)
//        if sender.selectedSegmentIndex == 0 {
//            currentPageType = .login
//        } else {
//            currentPageType = .signup
//        }
    }
    
    private func showErrorMessageIfNeeded(text: String?) {
        errorLabel.isHidden = text == nil
        errorLabel.text = text
    }
    
    @IBAction func signupButtonTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        if isSuccesfullLogin {
            delegate?.showMainTabBarController()
        } else {
            errorMessage = "Your credentials are invalid. Please try again!!!"
        }
    }
    
    @IBAction func forgotPasswordTapped(_ sender: UIButton) {
        
    }
}
