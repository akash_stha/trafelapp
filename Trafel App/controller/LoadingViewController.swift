//
//  LoadingViewController.swift
//  Trafel App
//
//  Created by Newarpunk on 4/3/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

class LoadingViewController: UIViewController {
    
    var isUserLoggedIn = false
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        delay(durationInSeconds: 1.0) {
            self.showInitialView()
        }
    }
    
    private func setUpView() {
        activityIndicator.style = .whiteLarge
        activityIndicator.color = .darkGray
    }
    
    private func showInitialView() {
        if isUserLoggedIn {
            PresenterManager.shared.show(vc: .mainTabBarViewController)
        } else {
            performSegue(withIdentifier: k.Segue.showOnboard, sender: nil)
        }
    }
    
}













//if let appDelegate = UIApplication.shared.delegate as? AppDelegate,
//    let window = appDelegate.window {
//    window.rootViewController = viewController
//    Trafel.transition(windowValue: window, seconds: 0.3, transitionStyle: .transitionCrossDissolve)
//}
