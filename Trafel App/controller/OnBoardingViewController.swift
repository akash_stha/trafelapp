//
//  OnBoardingViewController.swift
//  Trafel App
//
//  Created by Newarpunk on 4/3/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

protocol OnboardingDelegate: class {
    func showMainTabBarController()
}

class OnBoardingViewController: UIViewController {
    
    @IBOutlet weak var getstartedButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    var timer = Timer()
    var count: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageControl.numberOfPages = SlideModel.collection.count
        setupCollectionView()
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.changeImage), userInfo: nil, repeats: true)
        }
    }
    
    @objc func changeImage() {
        if count < SlideModel.collection.count {
            let index = IndexPath.init(item: count, section: 0)
            self.collectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            showCaptions(atIndex: count)
            pageControl.currentPage = count
            count += 1
        } else {
            count = 0
            let index = IndexPath.init(item: count, section: 0)
            self.collectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            showCaptions(atIndex: count)
            pageControl.currentPage = count
        }

    }
    
    private func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView.backgroundColor = .white
        collectionView.collectionViewLayout = layout
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        getstartedButton.layer.cornerRadius = getstartedButton.frame.height / 2
    }
    
    @IBAction func getStartedTapped(_ sender: UIButton) {
//        performSegue(withIdentifier: k.Segue.showLoginSignup, sender: nil)
//        let destination = LoginViewController()
//        destination.delegate = self
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == k.Segue.showLoginSignup {
//            if let destination = segue.destination as? LoginViewController {
//                destination.delegate = self
//            }
//        }
//    }
    
    private func showCaptions(atIndex index: Int) {
        let slide = SlideModel.collection[index]
        titleLabel.text = slide.title
        descriptionLabel.text = slide.description
    }
    
}

extension OnBoardingViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return SlideModel.collection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! OnBoardingCollectionViewCell
        let imageName = SlideModel.collection[indexPath.item].imageName
        let image = UIImage(named: imageName) ?? UIImage()
        cell.configureImage(image: image)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        let index = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
//        showCaptions(atIndex: index)
//        pageControl.currentPage = index
//        count = index
    }
}

extension OnBoardingViewController: OnboardingDelegate {
    func showMainTabBarController() {
        if let loginViewController = self.presentedViewController as? LoginViewController {
            loginViewController.dismiss(animated: true) {
                PresenterManager.shared.show(vc: .mainTabBarViewController)
            }
        }
    }
}

