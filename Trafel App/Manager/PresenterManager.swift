//
//  PresenterManager.swift
//  Trafel App
//
//  Created by Newarpunk on 4/3/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

class PresenterManager {
    
    static let shared = PresenterManager()
    
    private init() {}
    
    enum VC {
        case onBoarding
        case mainTabBarViewController
    }
    
    func show(vc: VC) {
        
        var viewController: UIViewController
        
        switch vc {
        case .onBoarding:
            viewController = UIStoryboard(name: k.StoryboardId.main, bundle: nil).instantiateViewController(withIdentifier: k.StoryboardId.onboardingViewController)
        case .mainTabBarViewController:
            viewController = UIStoryboard(name: k.StoryboardId.main, bundle: nil).instantiateViewController(withIdentifier: k.StoryboardId.mainTabBarController)
        }
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate,
            let window = appDelegate.window {
            window.rootViewController = viewController
            UIView.transition(with: window, duration: 0.3, options: .transitionCrossDissolve, animations: nil, completion: nil)
        }
    }
    
}
